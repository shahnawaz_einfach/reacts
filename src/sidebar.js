import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import { Link } from 'react-router-dom';


export default props => {
  return (
    <Menu>
      <Link className="menu-item" to="/Sidebar">
        Home
      </Link>

      <Link className="menu-item" to="/Register">
        Register
      </Link>

      <Link className="menu-item" to="/">
        login
      </Link>

      <Link className="menu-item" to="/DonationDetails">
        Donation Details
      </Link>

      <Link className="menu-item" to="/">
        About
      </Link>
    </Menu>
  );
};