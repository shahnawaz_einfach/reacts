import React from 'react';
import Navbar from './Components/Navbar';
import DonationDetails from './Components/DonationDetails';
import SideBar from './sidebar';
import Register from './Components/Register';
import './App.css';
import{
  BrowserRouter as Router,
  Switch,
  Route,
  Link
}from "react-router-dom";

export default function App() {

  return (
    <>
    {/* <Navbar/> */}
    
    <Router>
    <Navbar />
     <div id="App">
       <Switch>
          <Route path="/Register">
          <Register/>
          </Route>
          <Route path="/DonationDetails">
          <DonationDetails/>

          </Route>
          
          <Route path="/">
       <SideBar />
      <div id="page-wrap">
        <h1>Einfach charity</h1>
        <h2>We Help All People In Need</h2>
        
      </div>
      </Route>
      </Switch>
      
    </div> 
    </Router>
    
    </>
  );
}