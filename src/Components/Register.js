import React, { useState } from 'react'
import {nanoid} from 'nanoid';
import data from './mock-data.json';


export default function Register () {
    const [contacts,setContacts] = useState({data});
    const[addFormData,setAddFormData] = useState({
        fullName:'',
        address:'',
        phoneNumber:'',
        email:''
    })
    const handleAddFormChange = (event) => {
        event.preventDefault();

        const fieldName = event.target.getAttribute('name');
        const fieldValue = event.target.value;

        const newFormData = { ...addFormData};
        newFormData[fieldName] = fieldValue;

        setAddFormData(newFormData)
    };
    const handleAddFormSubmit = (event) => {
        event.preventDefault();

        const newContact = {
            id:nanoid(),
            fullName:addFormData.fullName,
            address:addFormData.address,
            phoneNumber:addFormData.phoneNumber,
            email:addFormData.email,

        };
        const newContacts = [...contacts,newContact];
        setContacts(newContacts )
        console.log(newContacts)
    };
    return (
        
        <div className="container">
            <form onSubmit={handleAddFormSubmit}>
                <div className="container">
                    <h1>Register</h1>
                    <p>Please fill in this form to create an account.</p>
                    <hr/>

                    <label for="name"><b>Name</b></label>
                    <input type="text" placeholder="Enter Name" onChange={handleAddFormChange} name="fullName" id="name" required/>

                    <label for="address"><b>Address</b></label>
                    <input type="text" placeholder="Enter Adress" onChange={handleAddFormChange} name="address" id="age" required/>

                    <label for="phoneNumber"><b>Phone  Number</b></label>
                    <input type="text" placeholder="Enter Phone Number" onChange={handleAddFormChange} name="phoneNumber" id="gender" required/>

                    <label for="email"><b>Email</b></label>
                    <input type="text" placeholder="Enter Email" onChange={handleAddFormChange} name="email" id="email" required/>

                   
                    <button type="submit"  className="registerbtn">Register</button>
                </div>
            </form>
        </div>

        
        
    )
}
